//
//  SimpleFeedbackFormExample.swift
//  FeedbackExample
//
//  Created by Konstantin on 04/03/2023.
//

import SwiftUI
import Zofx_Toolbox

struct SimpleFeedbackFormExample: View {
  @State var showFeedback: Bool = false
  var body: some View {
    VStack {
      Button("Send Feedback...", action: { showFeedback.toggle() })
    }
    .padding()
    .sheet(isPresented: $showFeedback, content: {PresentingSheetView()})
  }
}

struct PresentingSheetView: View {
  @Environment(\.dismiss) private var dismiss
  var body: some View {
    NavigationStack {
        //TODO: - Replace with your API key
        SimpleFeedbackForm(appKey: "0f287eb9-e644-4c01-bfe9-c4f28b019e27")
        .toolbar {
          ToolbarItem(placement: .cancellationAction) {
            Button("Dismiss", action: {dismiss()})
          }
        }
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    SimpleFeedbackFormExample()
  }
}
