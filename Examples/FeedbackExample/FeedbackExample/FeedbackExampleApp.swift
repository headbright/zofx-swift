//
//  FeedbackExampleApp.swift
//  FeedbackExample
//
//  Created by Konstantin on 04/03/2023.
//

import SwiftUI

@main
struct FeedbackExampleApp: App {
  var body: some Scene {
    WindowGroup {
      NavigationStack {
        SimpleFeedbackFormExample()
      }
    }
  }
}
