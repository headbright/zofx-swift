# Zofx

Client library allowing Swift applications to interact with [Zofx](https://codeberg.org/headbright/zofx). This package contains multiple targets you can choose to include depending on your needs:

- `Zofx.Toolbox` - A collection of feedback components ready to use in your app. Use this option to get started quickly using one of the production-ready designs developed by ZOFX.

- `Zofx` - base API client for the ZOFX API allowing you to manually submit feedback values. Use this options if you prefer to build your own UI experience for collecting feedback.

Quick start instructions and docs are [available here](https://zofx.eu/docs/platforms/swift/quick_start.html).

# License

    Copyright (C) 2023 Headbright Group, Konstantin hi@iamkonstantin.eu

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
