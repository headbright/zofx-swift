//
//  ImageUtils.swift
//
//
//  Created by Konstantin on 09/04/2023.
//

import SwiftUI

struct ImageUtils {

  #if canImport(AppKit)
    func jpegDataFrom(image: NSImage) -> Data {
      let cgImage = image.cgImage(forProposedRect: nil, context: nil, hints: nil)!
      let bitmapRep = NSBitmapImageRep(cgImage: cgImage)
      let jpegData = bitmapRep.representation(
        using: NSBitmapImageRep.FileType.jpeg, properties: [:])!
      return jpegData
    }
  #endif
}
